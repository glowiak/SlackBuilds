# SlackBuild Details

Name: oracle-jre8

Version: 1.8.0_181

Description: Java Binaries from Oracle

Depends on:

Homepage: http://java.com

Maintainer: glowiak

Download SlackBuild: [oracle-jre8.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/oracle-jre8.tar.gz)
