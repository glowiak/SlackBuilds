# SlackBuild Details

Name: gspi

Version: 0.2

Description: Graphical Slackware Package Installer is a tool for Slackware Linux that conbines features of both GDebi and Synaptic

Depends on: [yad](http://slackbuilds.org/repository/14.2/desktop/yad/) [sbotools](http://slackbuilds.org/repository/14.2/system/sbotools/)

Homepage: http://codeberg.org/glowiak/gspi

Maintainer: glowiak

Download SlackBuild: [gspi.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/gspi.tar.gz)
