# Packages

[ArialFont](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/ArialFont.md)

[MinecraftLauncher](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/MinecraftLauncher.md)

[firefox88](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/firefox88.md)

[lutris-beta](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/lutris-beta.md)

[makesbo](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/makesbo.md)

[pa-applet](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/pa-applet.md)

[arc-icon-theme](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/arc-icon-theme.md)

[openjdk16-bin](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/openjdk16-bin.md)

[oracle-jre8](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/oracle-jre8.md)

[polymc](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/polymc.md)

[gspi](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/gspi.md)

[adoptium-jdk8](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/adoptium-jdk8.md)

[polymc-edge](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/polymc-edge.md)

[adoptium-jdk17](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/adoptium-jdk17.md)
