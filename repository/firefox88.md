# SlackBuild Details

Name: firefox88

Version: 88.0

Description: Mozilla Firefox binaries (version 88.0)

Depends on:

Homepage: http://mozilla.org

Maintainer: glowiak

Download SlackBuild: [firefox88.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/firefox88.tar.gz)
