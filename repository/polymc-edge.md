# SlackBuild Details

Name: polymc-edge

Version: develop

Description: A custom launcher for Minecraft that allows you to easily manage multiple installations of Minecraft at once (Fork of MultiMC). This is always-newest version.

Depends on: [qt5](http://slackbuilds.org/repository/14.2/libraries/qt5/) [adoptium-jdk8](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/adoptium-jdk8.md)

Homepage: http://polymc.org

Maintainer: glowiak

Download SlackBuild: [polymc-edge.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/polymc-edge.tar.gz)
