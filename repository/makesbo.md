# SlackBuild Details

Name: makesbo

Version: 0.2

Description: arch makepkg-like tool to make easier building local SlackBuilds

Depends on: [sbotools](http://slackbuilds.org/repository/14.2/system/sbotools/) (optional, for dependiences support)

Homepage: http://codeberg.org/glowiak/makesbo

Maintainer: glowiak

Download SlackBuild: [makesbo.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/makesbo.tar.gz)
