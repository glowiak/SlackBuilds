# SlackBuild Details

Name: adoptium-jdk17

Version: 17.0.3_7

Description: JDK binaries from Adoptium. 64-bit only.

Depends on:

Homepage: http://adoptium.net

Maintainer: glowiak

Download SlackBuild: [adoptium-jdk17.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/adoptium-jdk17.tar.gz)
