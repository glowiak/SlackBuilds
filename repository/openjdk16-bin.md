# SlackBuild Details

Name: openjdk16-bin

Version: 16.0.2

Description: Repackaged ArchLinux32 OpenJDK16 Package. i686 only. For x86_64 install [jdk16](http://slackbuilds.org/repository/14.2/development/jdk16/?search=jdk16) Package. Contains both JRE and JDK. On 14.2 you need to upgrade glibc to 2.33 from current repositories. Do do an upgrade, download all files from [here](https://sourceforge.net/projects/ports-distfiles/files/GLIBC%202.33%20Binaries%20for%20Slackware%2014.2/) and run 'bash upgradegcc.sh' as root. This will broke gcc. To repair it type 'bash repairlibraries.sh'.

Depends on:

Homepage: http://openjdk.java.net

Maintainer: glowiak

Download SlackBuild: [openjdk16-bin.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/openjdk16-bin.tar.gz)
