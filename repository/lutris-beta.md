# SlackBuild Details

Name: lutris-beta

Version: 0.5.9-beta1

Description: Lutris (beta version)

Depends on: [pygobject3-python3](http://slackbuilds.org/repository/14.2/python/pygobject3-python3/?search=pygobject) [dbus-python3](http://slackbuilds.org/repository/14.2/python/dbus-python3/?search=dbus-python3) [python3-PyYAML](http://slackbuilds.org/repository/14.2/libraries/python3-PyYAML/?search=python3-PyYAML) [pyxdg](http://slackbuilds.org/repository/14.2/python/pyxdg/) [gnome-desktop](http://slackbuilds.org/repository/14.2/libraries/gnome-desktop/?search=gnome-desktop) [python3-pillow](http://slackbuilds.org/repository/14.2/libraries/python3-pillow/) [python-magic](http://slackbuilds.org/repository/14.2/python/python-magic/) [python-distro](http://slackbuilds.org/repository/14.2/python/python-distro/?search=python-distro) [meson](http://slackbuilds.org/repository/14.2/development/meson/?search=meson) [lxml](http://slackbuilds.org/repository/14.2/python/lxml/) [cabextract](http://slackbuilds.org/repository/14.2/system/cabextract/) [p7zip](http://slackbuilds.org/repository/14.2/system/p7zip/?search=p7zip) [python-evdev](http://slackbuilds.org/repository/14.2/python/python-evdev/?search=python-evdev) [python-requests](http://slackbuilds.org/repository/14.2/python/python-requests/) [webkit2gtk](http://slackbuilds.org/repository/14.2/libraries/webkit2gtk/?search=webkit2gtk)

Homepage: http://lutris.net

Maintainer: glowiak

Download SlackBuild: [lutris-beta.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/lutris-beta.tar.gz)
