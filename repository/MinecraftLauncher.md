# SlackBuild Details

Name: MinecraftLauncher

Version: debianbinary

Description: Unofficial SlackBuild of Minecraft Launcher (repackages debian package). Broken on 14.2

Depends on:

Homepage: http://minecraft.net

Maintainer: glowiak

Download SlackBuild: [MinecraftLauncher.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/MinecraftLauncher.tar.gz)
