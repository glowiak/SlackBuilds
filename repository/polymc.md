# SlackBuild Details

Name: polymc

Version: 1.3.1

Description: A custom launcher for Minecraft that allows you to easily manage multiple installations of Minecraft at once (Fork of MultiMC) 

Depends on: [qt5](http://slackbuilds.org/repository/14.2/libraries/qt5/) [adoptium-jdk8](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/adoptium-jdk8.md)

Homepage: http://polymc.org

Maintainer: glowiak

Download SlackBuild: [polymc.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/polymc.tar.gz)
