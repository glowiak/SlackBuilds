# SlackBuild Details

Name: arc-icon-theme

Version: 20161122

Description: The Arc Icon Theme

Depends on: [moka-icon-theme](http://slackbuilds.org/repository/14.2/desktop/moka-icon-theme/?search=moka-icon-theme)

Homepage: http://github.com/horst3180/arc-icon-theme

Maintainer: glowiak

Download SlackBuild: [arc-icon-theme.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/arc-icon-theme.tar.gz)
