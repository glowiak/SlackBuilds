# SlackBuild Details

Name: pa-applet

Version: master

Description: Simple applet for controlling PulseAudio sound. Very useful in tiling window managers

Depends on:

Homepage: http://github.com/fernandotcl/pa-applet

Maintainer: glowiak

Download SlackBuild: [pa-applet.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/pa-applet.tar.gz)
