# SlackBuild Details

Name: adoptium-jdk8

Version: 8u332b09

Description: JDK binaries from Adoptium. 64-bit only.

Depends on:

Homepage: http://adoptium.net

Maintainer: glowiak

Download SlackBuild: [adoptium-jdk8.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/adoptium-jdk8.tar.gz)
