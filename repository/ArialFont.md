# SlackBuild Details

Name: ArialFont

Version: 1.0

Description: Microsoft Fonts for Slackware Linux

Depends on:

Homepage: http://microsoft.com

Maintainer: glowiak

Download SlackBuild: [ArialFont.tar.gz](https://codeberg.org/glowiak/SlackBuilds/raw/branch/master/tgz/ArialFont.tar.gz)
