#!/bin/sh
CWD=$(pwd)
TGZs=$CWD/tgz
GITs=$CWD/git

cd $TGZs
rm -rf *.tar.gz
cd $GITs
for all in $(ls)
do
	echo "Packing: $all ---> $all.tar.gz"
	tar czvf $TGZs/$all.tar.gz $all
done
