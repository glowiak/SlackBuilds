# SlackBuilds

My SlackBuilds waiting (because official SlackBuilds.org closed submitting due to moving to Slackware 15.0)

## How to get it

You can download tar.gz archives from tgz directory or

clone this repo and access it directly via git directory.

## Will be in official SBo?

Yes, when they make support for Slackware 15.

### List of packages here

[List of SlackBuilds from this repo](https://codeberg.org/glowiak/SlackBuilds/src/branch/master/repository/head.md)
